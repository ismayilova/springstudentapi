package com.example.students.service;

import com.example.students.entity.Student;
import com.example.students.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class StudentService {
    @Autowired
   private StudentRepository studentRepository;

    public List<Student> getStudentsList(){
       List<Student> students = studentRepository.findAll();
        return students;
    }

    public Student getStudentById(Long id){

       return studentRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("No data found with given id = "+ id));

    }


    public Student addStudent(Student student){
          return  studentRepository.save(student);
    }

    public void updateStudentById( Long id , Student student){

                getStudentById(id);
                student.setId(id);
                studentRepository.save(student);

    }

    public void deleteStudent(Long id){
        studentRepository.delete(getStudentById(id));
    }



}
