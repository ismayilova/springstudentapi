package com.example.students.controllers;

import com.example.students.entity.Student;
import com.example.students.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/v1/students")
public class StudentController {
    @Autowired
    StudentService service;

    @GetMapping
    public ResponseEntity getStudents(){

        try{

            return new ResponseEntity(service.getStudentsList(), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @GetMapping("/{id}")
    public ResponseEntity getStudent(@PathVariable Long id){

        try{
            return new ResponseEntity(service.getStudentById(id), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/{id}")
    public ResponseEntity updateStudent( @PathVariable Long id , @RequestBody Student student){

        try{
            service.updateStudentById(id,student);
            return new ResponseEntity( HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteStudent(@PathVariable Long id){

        try {
            service.deleteStudent(id);
            return new ResponseEntity(HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping
    public ResponseEntity addStudent(@RequestBody Student student){


                   System.out.println(student.toString());
                   return  new ResponseEntity(service.addStudent(student),HttpStatus.CREATED);


    }

}
